# word文档PDF文档在线签章签名功能

#### Description
通过WEB网站后端实现word文档 pdf文档在线签名盖章能，可作为需要实现此功能的人员参考、学习。不足之处请谅解，本代码不可作为商业用途，不保证源码安全性。`

#### css样式

css样式基于Layui，本页面用到的样式有限，可自行参考Layui官方组件进行调整设置。
引入样式
<link rel="stylesheet" href="layui.css">

#### JS引用

1. pdfjs（pdfjs-dist）
2.  fabric
2.1 Pdfjs-dist 主要作用主要加载预览pdf文档
参考文档如下：

[https://gitcode.gitcode.host/docs-cn/pdf.js-docs-cn/index.html](http://)

pdf.js 的对象结构大致遵循实际 PDF 的结构。在顶层有一个文档对象。从文档中，可以获取更多信息和单独的页面。获取文档:

`pdfjsLib.getDocument('helloworld.pdf')`

请记住，虽然 pdf.js 使用 Promises ，并且上面将返回一个 PDFDocumentLoadingTask 实例，该实例具有 promise 属性，该属性是通过 Document 对象解析的。


```
var loadingTask = pdfjsLib.getDocument('helloworld.pdf');
loadingTask.promise.then(function(pdf) {
  // you can now use *pdf* here
});
```



#### Fabric库实现签名图章

参考文档

[https://www.fabricmc.net/](http://)

可在线调试

[http://fabricjs.com/custom-control-render](http://)

Fabric.js 是一个在服务器端运行的 Node.js 扩展模块，用于在 Web 上绘制各种图形的 JS 库。

#### 效果

![输入图片说明](2245459111814ca19701d0d2a94a7c24.png)

#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
